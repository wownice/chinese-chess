import java.util.ArrayList;

import javafx.animation.Interpolator;
import javafx.animation.PathTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.stage.Stage;
import javafx.util.Duration;

public class UI
{
	private Stage stage;
	private Scene scene;
	private Game game;
	private BoardTile[][] bTiles;//this covers all the clickable spaces
	private ImageView[][] pieces; //will only fill in spaces when needed
	private ArrayList<Piece> options;
	private Label turnLabel;
	private Label victoryLabel;
	private Image background = new Image("img/board.png");
	private HBox hBox;
	private Pane tileHolder;
	private Pane pieceHolder;
	private Pane clickHolder;
	private final int PIECE_SIZE = 67;
	
	public UI(Stage st)
	{
		stage = st;
		stage.setTitle("Game");
			
		newGame();		
		
		scene = new Scene(hBox);
		st.setScene(scene);
		st.show();	
	}
	
	public void newGame()
	{
		game = new Game();
		initializeGraphics();
		bTiles = new BoardTile[Board.BOARD_COLS][Board.BOARD_ROWS]; 
		pieces = new ImageView[Board.BOARD_COLS][Board.BOARD_ROWS];
		displayBoard();
	}
	
	public void initializeGraphics()
	{
		StackPane gamePane = new StackPane();
		tileHolder = new Pane();
		pieceHolder = new Pane();
		clickHolder = new Pane();
		gamePane.getChildren().addAll(tileHolder, pieceHolder, clickHolder);

		VBox vBox = new VBox();
		turnLabel = new Label();
		victoryLabel = new Label();
		
		updateTurnLabel();
		vBox.getChildren().add(turnLabel);
		vBox.getChildren().add(victoryLabel);
		
		for(Button b : getButtons())
		{
			vBox.getChildren().add(b);
		}			
	
		hBox = new HBox();
		hBox.getChildren().addAll(gamePane, vBox);
	}
	
	public void displayBoard()
	{				
		ImageView iv;
		for(int col = 0; col < game.getBoard().getPieces().length; col++)
		{
			for(int row = 0; row < game.getBoard().getPieces()[0].length; row++)
			{
				//background setup
				Piece p = game.getBoard().getPieces()[col][row];
				iv = new ImageView(background);
				iv.setViewport(new Rectangle2D(p.getX() * PIECE_SIZE % background.getWidth(),
						p.getY() * PIECE_SIZE, PIECE_SIZE, PIECE_SIZE));
				tileHolder.getChildren().add(iv);				
				iv.relocate(PIECE_SIZE * col,PIECE_SIZE * row);
				
				//piece setup
				pieces[col][row] = new ImageView();
				if(!p.isEmpty())
				{
					pieces[col][row].setImage(new Image("img/" + p.getName() + ".png"));					
				}
				pieces[col][row].relocate(PIECE_SIZE * col,PIECE_SIZE * row);
				pieceHolder.getChildren().add(pieces[col][row]);
				
				//clickable space setup
				bTiles[col][row] = new BoardTile(p);
				bTiles[col][row].relocate(PIECE_SIZE * col,PIECE_SIZE * row);
				clickHolder.getChildren().add(bTiles[col][row]);
			}
		}
	}
	
	public void makeMove(Piece p)
	{		
		clearAvailableOptions();
		Piece selectedPiece = game.getSelectedPiece();
		Path path = new Path();
		path.getElements().add(new MoveToAbs(pieces[selectedPiece.getX()][selectedPiece.getY()], 
				pieces[selectedPiece.getX()][selectedPiece.getY()].getLayoutX(), 
				pieces[selectedPiece.getX()][selectedPiece.getY()].getLayoutY()));
		
        path.getElements().add(new LineToAbs(pieces[selectedPiece.getX()][selectedPiece.getY()], 
        		pieces[selectedPiece.getX()][selectedPiece.getY()].getLayoutX()
        		+ ((p.getX() - selectedPiece.getX()) * PIECE_SIZE), 
        		pieces[selectedPiece.getX()][selectedPiece.getY()].getLayoutY()
        		+ ((p.getY() - selectedPiece.getY()) * PIECE_SIZE)));
        
        PathTransition pathTransition = new PathTransition();
        pathTransition.setDuration(Duration.millis(100));
        pathTransition.setInterpolator(Interpolator.EASE_BOTH);
        pathTransition.setNode(pieces[selectedPiece.getX()][selectedPiece.getY()]);
        pathTransition.setPath(path);
        
        //moved piece will always be above any pieces it goes through
        pieces[selectedPiece.getX()][selectedPiece.getY()].toFront(); 

        pathTransition.setOnFinished(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent event) 
            {
            	game.movePieceAndUpdate(game.getSelectedPiece(), p);
        		updateBoard();	
        		game.switchPlayerTurnAndUpdate();
        		clearSelection();
        		playChangeTurnSound();
        		victoryCheck();
            }
        });
        pathTransition.play();
	}
	
	public void playPieceSelectSound(Piece piece)
	{
		Media media;
		if(!piece.getTeamString().equals(game.getPlayerTurn()))
		{
			media = new Media(getClass().getClassLoader()
		             .getResource("sfx/selectunit.mp3").toString());
			
		}
		else
		{
			media = new Media(getClass().getClassLoader()
		             .getResource("sfx/selectpiece.mp3").toString());
		}
		MediaPlayer mediaPlayer = new MediaPlayer(media);
		mediaPlayer.play();
	}
	
	public void playChangeTurnSound()
	{
		Media media;
		if(game.getPlayerTurn().equals("b"))
		{
			media = new Media(getClass().getClassLoader()
		             .getResource("sfx/p2phase.mp3").toString());
		}
		else
		{
			media = new Media(getClass().getClassLoader()
		             .getResource("sfx/p1phase.mp3").toString());
		}
		MediaPlayer mediaPlayer = new MediaPlayer(media);
		mediaPlayer.play();
	}
	
	public void showAvailableOptions()
	{
		Color c; 
		
		if(game.getPlayerTurn().equals(game.getSelectedPiece().getTeamString()))
		{
			c = new Color(0,0,1,0.5);
		}
		else
		{
			c = new Color(1,0,0,0.5);
		}
		
		options = game.getOptionsForPiece(game.getSelectedPiece());
		for(Piece p : options)
		{
			if(game.isLegalMove(game.getSelectedPiece(), p))
			{
				bTiles[p.getX()][p.getY()].setBackground(new Background(new BackgroundFill(
                    c, CornerRadii.EMPTY, Insets.EMPTY)));
			}
		}
	}
	
	public void clearAvailableOptions()
	{
		for(Piece p : options)
		{
			bTiles[p.getX()][p.getY()].setBackground(null);
		}
	}
	
	public void clearSelection()
	{
		clearAvailableOptions();
		pieces[game.getSelectedPiece().getX()][game.getSelectedPiece().getY()].setEffect(null);
		game.setSelectedPiece(null);
	}
	
	public ArrayList<Button> getButtons()
	{
		ArrayList<Button> buttons = new ArrayList<Button>();
		
		Button undoButton = new Button("Undo");
		undoButton.setOnAction(new EventHandler<ActionEvent>() 
		{
			public void handle(ActionEvent e) 
			{
				if(game.getSelectedPiece() != null)
				{
					clearSelection();					
				}

				game.undoMove();
				updateBoard();
				victoryCheck();
			}
		});
		buttons.add(undoButton);
				
		return buttons;
	}
	
	public void updateBoard()
	{
		if(!game.getCurrentTurnChanges().isEmpty())
		{
			int x;
			int y;
			
			for(Piece p : game.getCurrentTurnChanges())
			{
				x = p.getX();
				y = p.getY();
	        	
				if(!p.isEmpty())
		        {
		        	pieces[x][y].setImage(new Image("img/" + p.getName() + ".png"));
		        }
		        else
		        {
		        	pieces[x][y].setImage(null);
		        }
				//reset the imageview's coordinates after a pathtransition
				pieces[x][y].setTranslateX(0);
				pieces[x][y].setTranslateY(0);
				bTiles[x][y].updatePiece(p);
			}
		}
		updateTurnLabel();
	}
	
	public void victoryCheck()
	{
		if(game.checkmate("b"))
		{
			victoryLabel.setText("Red Wins!");
		}
		else if(game.checkmate("r"))
		{
			victoryLabel.setText("Black Wins!");
		}
		else
		{
			victoryLabel.setText("");
		}
	}
	
	public void updateTurnLabel()
	{
		turnLabel.setText("Current Turn: " + game.getCurrentTurn());
	}
	
	public class BoardTile extends StackPane
	{
		private Piece p;
		
		public BoardTile(Piece piece)
		{			 
			updatePiece(piece);
	        setMinSize(67,67);
			//setStyle("-fx-border-color: black");	       
		}
		
		public void updateClickOptions()
		{
			DropShadow selectShadow = new DropShadow();		
			selectShadow.setRadius(5.0);
			selectShadow.setOffsetX(3.0);
			selectShadow.setOffsetY(3.0);
			selectShadow.setColor(Color.color(0.4, 0.5, 0.5));
	        setOnMouseClicked(new EventHandler<MouseEvent>() 
			{
				@Override
				public void handle(MouseEvent event) 
				{
					if(game.getSelectedPiece() != null && 
							game.getPlayerTurn().equals(game.getSelectedPiece().getTeamString())
							&& game.isLegalMove(game.getSelectedPiece(), p))
					{
						makeMove(p);
					}
					else if(!p.isEmpty())
					{
						if(game.getSelectedPiece() != null)
						{
							clearSelection();
						}
						game.setSelectedPiece(p);
						
						pieces[p.getX()][p.getY()].setEffect(selectShadow);
						
						showAvailableOptions();							
						playPieceSelectSound(p);
					}
					else if(game.getSelectedPiece() != null)
					{
						if(game.getPlayerTurn().equals(game.getSelectedPiece().getTeamString()) 
								&& game.isLegalMove(game.getSelectedPiece(),p))
						{
							makeMove(p);
						}
						else
						{
							clearSelection();
						}
					}
				}
			});
		}
		
		public Piece getPiece()
		{
			return p;
		}
		
		public void updatePiece(Piece newP)
		{
			p = new Piece(newP);
			updateClickOptions();
		}
	}

	public static class MoveToAbs extends MoveTo
	{
        public MoveToAbs(Node node) {
            super(node.getLayoutBounds().getWidth() / 2, node.getLayoutBounds().getHeight() / 2);
        }

        public MoveToAbs(Node node, double x, double y) {
            super(x - node.getLayoutX() + node.getLayoutBounds().getWidth() / 2, y - node.getLayoutY() + node.getLayoutBounds().getHeight() / 2);
        }

    }

    public static class LineToAbs extends LineTo 
    {
        public LineToAbs(Node node, double x, double y) {
            super(x - node.getLayoutX() + node.getLayoutBounds().getWidth() / 2, y - node.getLayoutY() + node.getLayoutBounds().getHeight() / 2);
        }
    }
}