import java.util.ArrayList;

public class Board 
{
	static final int BOARD_COLS = 9; //x
	static final int BOARD_ROWS = 10; //y
	static final int PLAYER1_RIVER_Y = 5;
	static final int PLAYER2_RIVER_Y = 4;
	private Piece[][] gamePieces;
	//Piece[x][y] x>>> yVVV row VVV col >>>
	private ArrayList<Piece> player1Pieces;
	private ArrayList<Piece> player2Pieces;
	
	public Board()
	{
		gamePieces = new Piece[BOARD_COLS][BOARD_ROWS];
		initBoard();
	}
	
	public Piece[][] getPieces()
	{
		return gamePieces;
	}
	
	public void movePiece(Piece currentPiece, Piece targetPiece)
	{
		int oldX = currentPiece.getX();
		int oldY = currentPiece.getY();	
		int newX = targetPiece.getX();
		int newY = targetPiece.getY();

		gamePieces[newX][newY].setName(currentPiece.getName());
		gamePieces[oldX][oldY].clearPiece();
	}
	
	public void initBoard()
	{		
		emptyBoard();
		
		player1Pieces = new ArrayList<Piece>();
		player2Pieces = new ArrayList<Piece>();
		
		//red pieces
		player1Pieces.add(new Piece("rj",0,9));
		player1Pieces.add(new Piece("rm",1,9));
		player1Pieces.add(new Piece("rx",2,9));
		player1Pieces.add(new Piece("rs",3,9));
		player1Pieces.add(new Piece("rb",4,9));
		player1Pieces.add(new Piece("rs",5,9));
		player1Pieces.add(new Piece("rx",6,9));
		player1Pieces.add(new Piece("rm",7,9));
		player1Pieces.add(new Piece("rj",8,9));
		player1Pieces.add(new Piece("rp",1,7));
		player1Pieces.add(new Piece("rp",7,7));
		player1Pieces.add(new Piece("rz",0,6));
		player1Pieces.add(new Piece("rz",2,6));
		player1Pieces.add(new Piece("rz",4,6));
		player1Pieces.add(new Piece("rz",6,6));
		player1Pieces.add(new Piece("rz",8,6));
		
		//black pieces
		player2Pieces.add(new Piece("bj",0,0));
		player2Pieces.add(new Piece("bm",1,0));
		player2Pieces.add(new Piece("bx",2,0));
		player2Pieces.add(new Piece("bs",3,0));
		player2Pieces.add(new Piece("bb",4,0));
		player2Pieces.add(new Piece("bs",5,0));
		player2Pieces.add(new Piece("bx",6,0));
		player2Pieces.add(new Piece("bm",7,0));
		player2Pieces.add(new Piece("bj",8,0));
		player2Pieces.add(new Piece("bp",1,2));
		player2Pieces.add(new Piece("bp",7,2));
		player2Pieces.add(new Piece("bz",0,3));
		player2Pieces.add(new Piece("bz",2,3));
		player2Pieces.add(new Piece("bz",4,3));
		player2Pieces.add(new Piece("bz",6,3));
		player2Pieces.add(new Piece("bz",8,3));
		
		for(Piece p : player1Pieces)
		{
			gamePieces[p.getX()][p.getY()] = p;
		}
		
		for(Piece p : player2Pieces)
		{
			gamePieces[p.getX()][p.getY()] = p;
		}
		
		/*
		//red pieces
		gamePieces[0][9] = new Piece("rj",0,9);
		gamePieces[1][9] = new Piece("rm",1,9);
		gamePieces[2][9] = new Piece("rx",2,9);
		gamePieces[3][9] = new Piece("rs",3,9);
		gamePieces[4][9] = new Piece("rb",4,9);
		gamePieces[5][9] = new Piece("rs",5,9);
		gamePieces[6][9] = new Piece("rx",6,9);
		gamePieces[7][9] = new Piece("rm",7,9);
		gamePieces[8][9] = new Piece("rj",8,9);	
		gamePieces[1][7] = new Piece("rp",1,7);
		gamePieces[7][7] = new Piece("rp",7,7);
		gamePieces[0][6] = new Piece("rz",0,6);
		gamePieces[2][6] = new Piece("rz",2,6);
		gamePieces[4][6] = new Piece("rz",4,6);
		gamePieces[6][6] = new Piece("rz",6,6);
		gamePieces[8][6] = new Piece("rz",8,6);
				
		//black pieces
		gamePieces[0][0] = new Piece("bj",0,0);
		gamePieces[1][0] = new Piece("bm",1,0);
		gamePieces[2][0] = new Piece("bx",2,0);
		gamePieces[3][0] = new Piece("bs",3,0);
		gamePieces[4][0] = new Piece("bb",4,0);
		gamePieces[5][0] = new Piece("bs",5,0);
		gamePieces[6][0] = new Piece("bx",6,0);
		gamePieces[7][0] = new Piece("bm",7,0);
		gamePieces[8][0] = new Piece("bj",8,0);		
		gamePieces[1][2] = new Piece("bp",1,2);
		gamePieces[7][2] = new Piece("bp",7,2);	
		gamePieces[0][3] = new Piece("bz",0,3);
		gamePieces[2][3] = new Piece("bz",2,3);
		gamePieces[4][3] = new Piece("bz",4,3);
		gamePieces[6][3] = new Piece("bz",6,3);
		gamePieces[8][3] = new Piece("bz",8,3);		
		*/
	}
	
	public void emptyBoard()
	{
		for(int i = 0; i < BOARD_COLS; i++)
		{
			for(int j = 0; j < BOARD_ROWS; j++)
			{
				gamePieces[i][j] = new Piece(i,j);
			}
		}
	}
	
	public ArrayList<Piece> getTeamPieces(String team)
	{
		if(team.equals("b"))
		{
			return player2Pieces;
		}
		return player1Pieces;
	}
	
	public ArrayList<Piece> getPlayer1Pieces()
	{
		return player1Pieces;
	}
	
	public ArrayList<Piece> getPlayer2Pieces()
	{
		return player2Pieces;
	}
	
	public void updateTeamPieces(ArrayList<Piece> pieces)
	{
		if(pieces.get(0).getTeamString().equals("b"))
		{
			player2Pieces = new ArrayList<Piece>();
			for(Piece p : pieces)
			{
				player2Pieces.add(p);
			}
		}
		else
		{
			player1Pieces = new ArrayList<Piece>();
			for(Piece p : pieces)
			{
				player1Pieces.add(p);
			}
		}
	}
}
