import java.util.ArrayList;

public class Game
{
	private Board board;
	private Piece selectedPiece; //the player's selected piece
	private String currentPlayerTurn = "";  //player1 = r, player2 = b
	private ArrayList<ArrayList<Piece>> changedPieces; //array of oldPieces
	private ArrayList<Piece> currentTurnChanges; //pieces that were moved this turn
	private ArrayList<Piece> oldPieces; //pieces before they were moved
	private Piece player1General;
	private Piece player2General;
	private int currentTurn;
	private String humanPlayerTeam;
	
	public Game()
	{
		board = new Board();
		changedPieces = new ArrayList<ArrayList<Piece>>();
		currentTurnChanges = new ArrayList<Piece>();
		oldPieces = new ArrayList<Piece>();
		updateGeneral(board.getPieces()[4][9]);
		updateGeneral(board.getPieces()[4][0]);
		currentTurn = 1;
		setPlayerTurn("r");
		setHumanPlayerTeam("r");
	}
	
	public void movePieceAndUpdate(Piece piece, Piece targetPiece) //this adds to the changedPieces array
	{	
		oldPieces.add(new Piece(piece));	
		oldPieces.add(new Piece(targetPiece));
		changedPieces.add(oldPieces);
		
		board.movePiece(piece, targetPiece);
		
		//new "current" piece, target piece's x and y
		currentTurnChanges.add(new Piece(board.getPieces()[targetPiece.getX()][targetPiece.getY()]));
		//the old current piece's x and y, now cleared
		currentTurnChanges.add(new Piece(board.getPieces()[piece.getX()][piece.getY()]));
		
		updateGeneral(currentTurnChanges.get(0));
		updatePieceArrays();
		oldPieces = new ArrayList<Piece>();
		if(currentPlayerTurn.equals("b"))
		{
			currentTurn++;
		}
		
		board.movePiece(player1General, player1General);
	}
	
	public ArrayList<Piece> getOptionsForPiece(Piece piece)
	{
		ArrayList<Piece> options = new ArrayList<Piece>();
		
		String pieceSide = piece.getTeamString(); //the team the piece is on
		String pieceType = piece.getTypeString(); //the kind of piece it is (general,soldier)
		int sideDecider = 1; //red, player1 (bottom)
		
		if(pieceSide.equals("b")) //black, player2 (top)
		{
			sideDecider = -1;
		}

		switch(pieceType)
		{
			case "b": //general
				generalOptions(piece, options, sideDecider);
				break;
			case "j": //chariot						
				chariotOptions(piece, options);
				break;
			case "m": //horse
				horseOptions(piece, options, sideDecider);
				break;
			case "p": //cannon
				cannonOptions(piece, options);
				break;
			case "s": //advisor
				advisorOptions(piece, options, sideDecider);
				break;
			case "x": //elephant
				elephantOptions(piece, options, sideDecider);
				break;
			case "z": //soldier
				soldierOptions(piece, options, sideDecider);
				break;
		}
		return options;
	}		
	
	public void horseOptions(Piece piece, ArrayList<Piece> options, int sideDecider)
	{
		int x = piece.getX();
		int y = piece.getY() - sideDecider;		
		Piece tempPiece = null;
		
		if(x >= 0 && x <= 8 && y >= 0 && y <= 9)
		{
			tempPiece = board.getPieces()[x][y];
			if(tempPiece.isEmpty())
			{
				x = piece.getX() - (1 * sideDecider);
				y = piece.getY() - (2 * sideDecider);				
				if(x >= 0 && x <= 8 && y >= 0 && y <= 9)
				{
					tempPiece = new Piece(board.getPieces()[x][y]);
					if(isMovable(piece, tempPiece)) //forward left
					{
						options.add(tempPiece);
					}
				}
				
				x = piece.getX() - (-1 * sideDecider);
				if(x >= 0 && x <= 8 && y >= 0 && y <= 9)
				{
					tempPiece = new Piece(board.getPieces()[x][y]);
					if(isMovable(piece, tempPiece)) //forward right
					{
						options.add(tempPiece);
					}
				}
			}
		}
		
		x = piece.getX();
		y = piece.getY() + sideDecider;
		if(x >= 0 && x <= 8 && y >= 0 && y <= 9)
		{
			tempPiece = board.getPieces()[x][y];
			if(tempPiece.isEmpty())
			{
				x = piece.getX() - (-1 * sideDecider);
				y = piece.getY() + (2 * sideDecider);
				if(x >= 0 && x <= 8 && y >= 0 && y <= 9)
				{
					tempPiece = new Piece(board.getPieces()[x][y]);
					if(isMovable(piece, tempPiece)) //back right
					{
						options.add(tempPiece);
					}
				}
				
				x = piece.getX() - (1 * sideDecider);
				if(x >= 0 && x <= 8 && y >= 0 && y <= 9)
				{
					tempPiece = new Piece(board.getPieces()[x][y]);
					if(isMovable(piece, tempPiece)) //back left
					{
						options.add(tempPiece);
					}
				}
			}
		}
	}
	
	public void advisorOptions(Piece piece, ArrayList<Piece> options, int sideDecider)
	{
		int x;
		int y;
		
		x = piece.getX() - (1 * sideDecider);
		y = piece.getY() - (1 * sideDecider);
		Piece tempPiece = null;
		if(x >= 3 && x <= 5 && ((sideDecider == 1 && (y >= 7) ||
				sideDecider == -1 && y <= 2)))
		{
			tempPiece = new Piece(board.getPieces()[x][y]);
			if(isMovable(piece, tempPiece)) //forward left
			{
				options.add(tempPiece);
			}
		}
		
		x = piece.getX() - (-1 * sideDecider);
		if(x >= 3 && x <= 5 && ((sideDecider == 1 && (y >= 7) ||
				sideDecider == -1 && y <= 2)))
		{
			tempPiece = new Piece(board.getPieces()[x][y]);
			if(isMovable(piece, tempPiece)) //forward right
			{
				options.add(tempPiece);
			}
		}
		
		y = piece.getY() + (1 * sideDecider);
		if(x >= 3 && x <= 5 && ((sideDecider == 1 && y <= 9) ||
				(sideDecider == -1 && y >= 0)))
		{
			tempPiece = new Piece(board.getPieces()[x][y]);
			if(isMovable(piece, tempPiece)) //back right
			{
				options.add(tempPiece);
			}
		}
		
		x = piece.getX() - (1 * sideDecider);
		if(x >= 3 && x <= 5 && ((sideDecider == 1 && y <= 9) ||
				(sideDecider == -1 && y >= 0)))
		{
			tempPiece = new Piece(board.getPieces()[x][y]);
			if(isMovable(piece, tempPiece)) //back left
			{
				options.add(tempPiece);
			}
		}
	}
	
	public void elephantOptions(Piece piece, ArrayList<Piece> options, int sideDecider)
	{
		int x;
		int y;
		int blockerX;
		int blockerY;
		
		x = piece.getX() - (2 * sideDecider);
		y = piece.getY() - (2 * sideDecider);
		
		blockerX = piece.getX() - (1 * sideDecider);
		blockerY = piece.getY() - (1 * sideDecider);
		Piece tempPiece = null;
		
		if(x >= 0 && x <= 8 && ((sideDecider == 1 && y >= Board.PLAYER1_RIVER_Y) ||
				sideDecider == -1 && y <= Board.PLAYER2_RIVER_Y) 
				&& board.getPieces()[blockerX][blockerY].isEmpty())
		{
			tempPiece = new Piece(board.getPieces()[x][y]);
			if(isMovable(piece, tempPiece)) //forward left
			{
				options.add(tempPiece);
			}
		}
		
		x = piece.getX() - (-2 * sideDecider);
		blockerX = piece.getX() - (-1 * sideDecider);
		if(x >= 0 && x <= 8 && ((sideDecider == 1 && y >= Board.PLAYER1_RIVER_Y) ||
				sideDecider == -1 && y <= Board.PLAYER2_RIVER_Y) 
				&& board.getPieces()[blockerX][blockerY].isEmpty())
		{
			tempPiece = new Piece(board.getPieces()[x][y]);
			if(isMovable(piece, tempPiece)) //forward right
			{
				options.add(tempPiece);
			}
		}
		
		x = piece.getX() - (2 * sideDecider);
		y = piece.getY() + (2 * sideDecider);
		blockerX = piece.getX() - (1 * sideDecider);
		blockerY = piece.getY() + (1 * sideDecider);
		if(x >= 0 && x <= 8 && y >= 0 && y <= 9 && board.getPieces()[blockerX][blockerY].isEmpty())
		{
			tempPiece = new Piece(board.getPieces()[x][y]);
			if(isMovable(piece, tempPiece)) //back left
			{
				options.add(tempPiece);
			}
		}
		
		x = piece.getX() + (2 * sideDecider);
		blockerX = piece.getX() + (1 * sideDecider);
		if(x >= 0 && x <= 8 && y >= 0 && y <= 9 && board.getPieces()[blockerX][blockerY].isEmpty())
		{
			tempPiece = new Piece(board.getPieces()[x][y]);
			if(isMovable(piece, tempPiece)) //back right
			{
				options.add(tempPiece);
			}
		}				
	}
	
	public void cannonOptions(Piece piece, ArrayList<Piece> options)
	{
		int currentPieceX = piece.getX();
		int currentPieceY = piece.getY();
		
		int counter = currentPieceX - 1; //left
		boolean movesExist = true;
		Piece tempPiece = null;
		while(counter >= 0 && movesExist)
		{
			tempPiece = new Piece(board.getPieces()[counter][currentPieceY]);
			if(isMovable(piece, tempPiece))
			{
				if(areOnDifferentTeams(piece, tempPiece))
				{
					movesExist = false;
				}
				else
				{
					options.add(tempPiece);
				}
			}
			else
			{
				movesExist = false;
			}
			counter--;
		}	
		
		if(tempPiece != null && tempPiece.getX() > 0) //left jumping
		{
			boolean passedMiddlePiece = false;
			counter = tempPiece.getX();
			movesExist = true;
			
			while(counter >= 0 && movesExist) 
			{
				tempPiece = new Piece(board.getPieces()[counter][currentPieceY]);
				if(passedMiddlePiece && areOnDifferentTeams(piece, tempPiece))
				{
					options.add(tempPiece);
					movesExist = false;
				}
				else if(passedMiddlePiece && !tempPiece.isEmpty())
				{
					movesExist = false;
				}
				if(!tempPiece.isEmpty())
				{
					passedMiddlePiece = true;
				}
				counter--;
			}		
		}
		
		counter = currentPieceX + 1; //right
		movesExist = true;
		tempPiece = null;
		while(counter < Board.BOARD_COLS && movesExist)
		{
			tempPiece = new Piece(board.getPieces()[counter][currentPieceY]);
			if(isMovable(piece, tempPiece))
			{
				if(areOnDifferentTeams(piece, tempPiece))
				{
					movesExist = false;
				}
				else
				{
					options.add(tempPiece);
				}
			}
			else
			{
				movesExist = false;
			}
			counter++;
		}
		
		if(tempPiece != null && tempPiece.getX() < 8) //right jumping
		{
			boolean passedMiddlePiece = false;
			counter = tempPiece.getX();
			movesExist = true;
			
			while(counter <= 8 && movesExist)
			{
				tempPiece = new Piece(board.getPieces()[counter][currentPieceY]);
				if(passedMiddlePiece && areOnDifferentTeams(piece, tempPiece))
				{
					options.add(tempPiece);
					movesExist = false;
				}
				else if(passedMiddlePiece && !tempPiece.isEmpty())
				{
					movesExist = false;
				}
				if(!tempPiece.isEmpty())
				{
					passedMiddlePiece = true;
				}
				counter++;
			}
		}
		
		counter = currentPieceY - 1; //top
		movesExist = true;
		tempPiece = null;
		while(counter >= 0 && movesExist)
		{
			tempPiece = new Piece(board.getPieces()[currentPieceX][counter]);
			if(isMovable(piece, tempPiece))
			{
				if(areOnDifferentTeams(piece, tempPiece))
				{
					movesExist = false;
				}
				else
				{
					options.add(tempPiece);
				}
			}
			else
			{
				movesExist = false;
			}
			counter--;
		}
		
		if(tempPiece != null && tempPiece.getY() > 0) //top jumping
		{
			boolean passedMiddlePiece = false;
			counter = tempPiece.getY();
			movesExist = true;
			while(counter >= 0 && movesExist)
			{	
				tempPiece = new Piece(board.getPieces()[currentPieceX][counter]);
				if(passedMiddlePiece && areOnDifferentTeams(piece, tempPiece))
				{
					options.add(tempPiece);
					movesExist = false;
				}
				else if(passedMiddlePiece && !tempPiece.isEmpty())
				{
					movesExist = false;
				}
				if(!tempPiece.isEmpty())
				{
					passedMiddlePiece = true;
				}
				counter--;
			}
		}
		counter = currentPieceY + 1; //bottom
		movesExist = true;
		tempPiece = null;
		while(counter < Board.BOARD_ROWS && movesExist)
		{
			tempPiece = new Piece(board.getPieces()[currentPieceX][counter]);
			if(isMovable(piece, tempPiece))
			{
				if(areOnDifferentTeams(piece, tempPiece))
				{
					movesExist = false;
				}
				else
				{
					options.add(tempPiece);
				}
			}
			else
			{
				movesExist = false;
			}
			counter++;
		}
		
		if(tempPiece != null && tempPiece.getY() < 9) //bottom jumping
		{
			boolean passedMiddlePiece = false;
			counter = tempPiece.getY();
			movesExist = true;
			while(counter <= 9 && movesExist)
			{
				tempPiece = new Piece(board.getPieces()[currentPieceX][counter]);
				if(passedMiddlePiece && areOnDifferentTeams(piece, tempPiece))
				{
					options.add(tempPiece);
					movesExist = false;
				}
				else if(passedMiddlePiece && !tempPiece.isEmpty())
				{
					movesExist = false;
				}
				if(!tempPiece.isEmpty())
				{
					passedMiddlePiece = true;
				}
				counter++;
			}
		}
	}
	
	public void generalOptions(Piece piece, ArrayList<Piece> options, int sideDecider)
	{
		int currentPieceX = piece.getX();
		int currentPieceY = piece.getY();
		Piece tempPiece;
		
		//left and right		
		if(currentPieceX > 3 && isMovable(piece, board.getPieces()[currentPieceX - 1][currentPieceY]))
		{
			tempPiece = new Piece(board.getPieces()[currentPieceX - 1][currentPieceY]);
			options.add(tempPiece);
		}
		
		if(currentPieceX < 5 && isMovable(piece, board.getPieces()[currentPieceX + 1][currentPieceY]))
		{
			tempPiece = new Piece(board.getPieces()[currentPieceX + 1][currentPieceY]);
			options.add(tempPiece);
		}
		
		//top and bottom
		if(sideDecider == 1)
		{			
			if(currentPieceY > 7 && currentPieceY <= 9 &&
					isMovable(piece, board.getPieces()[currentPieceX][currentPieceY - 1]))//up
			{				
				tempPiece = new Piece(board.getPieces()[currentPieceX][currentPieceY - 1]);
				options.add(tempPiece);
			}

			if(currentPieceY > 6 && currentPieceY < 9 &&
					isMovable(piece, board.getPieces()[currentPieceX][currentPieceY + 1]))//down
			{
				tempPiece = new Piece(board.getPieces()[currentPieceX][currentPieceY + 1]);
				options.add(tempPiece);
			}
		}
		else
		{			
			if(currentPieceY < 2 && currentPieceY >= 0 &&
					isMovable(piece, board.getPieces()[currentPieceX][currentPieceY + 1])) //up (down)
			{
				tempPiece = new Piece(board.getPieces()[currentPieceX][currentPieceY + 1]);
				options.add(tempPiece);
			}
			
			if(currentPieceY < 3 && currentPieceY > 0 && 
					isMovable(piece, board.getPieces()[currentPieceX][currentPieceY - 1])) //down (up)
			{
				tempPiece = new Piece(board.getPieces()[currentPieceX][currentPieceY - 1]);
				options.add(tempPiece);
			}
		}
		
		if(generalEyeContact())
		{
			if(piece.getTeamString().equals("b"))
			{
				options.add(player1General);
			}
			else
			{
				options.add(player2General);
			}
		}
	}
	
	public void chariotOptions(Piece piece, ArrayList<Piece> options)
	{
		int currentPieceX = piece.getX();
		int currentPieceY = piece.getY();
		
		int counter = currentPieceX - 1; //left
		boolean movesExist = true;
		Piece tempPiece;
		while(counter >= 0 && movesExist)
		{
			tempPiece = new Piece(board.getPieces()[counter][currentPieceY]);
			if(isMovable(piece, tempPiece))
			{
				if(areOnDifferentTeams(piece, tempPiece))
				{
					movesExist = false;
				}
				options.add(tempPiece);
			}
			else
			{
				movesExist = false;
			}
			counter--;
		}

		counter = currentPieceX + 1; //right
		movesExist = true;
		while(counter < Board.BOARD_COLS && movesExist)
		{
			tempPiece = new Piece(board.getPieces()[counter][currentPieceY]);
			if(isMovable(piece, tempPiece))
			{
				if(areOnDifferentTeams(piece, tempPiece))
				{
					movesExist = false;
				}
				options.add(tempPiece);
			}
			else
			{
				movesExist = false;
			}
			counter++;
		}
		
		counter = currentPieceY - 1; //top
		movesExist = true;
		while(counter >= 0 && movesExist)
		{
			tempPiece = new Piece(board.getPieces()[currentPieceX][counter]);
			if(isMovable(piece, tempPiece))
			{
				if(areOnDifferentTeams(piece, tempPiece))
				{
					movesExist = false;
				}
				options.add(tempPiece);
			}
			else
			{
				movesExist = false;
			}
			counter--;
		}

		counter = currentPieceY + 1; //bottom
		movesExist = true;
		while(counter < Board.BOARD_ROWS && movesExist)
		{
			tempPiece = new Piece(board.getPieces()[currentPieceX][counter]);
			if(isMovable(piece, tempPiece))
			{
				if(areOnDifferentTeams(piece, tempPiece))
				{
					movesExist = false;
				}
				options.add(tempPiece);
			}
			else
			{
				movesExist = false;
			}
			counter++;
		}	
	}
	
	public void soldierOptions(Piece piece, ArrayList<Piece> options, int sideDecider)
	{
		int currentPieceX = piece.getX();
		int currentPieceY = piece.getY();
		Piece tempPiece;
		
		if((currentPieceY > 0 && sideDecider == 1) || (currentPieceY < 8 && sideDecider == -1))
		{
			tempPiece = board.getPieces()[currentPieceX][currentPieceY - sideDecider];
			if(isMovable(piece, tempPiece))
			{
				options.add(tempPiece);
			}
		}
		
		if((sideDecider == 1 && piece.getY() <= Board.PLAYER2_RIVER_Y) ||
				(sideDecider == -1 && piece.getY() >= Board.PLAYER1_RIVER_Y))
		{			
			if(currentPieceX > 0)
			{
				tempPiece = board.getPieces()[currentPieceX - 1][currentPieceY];
				if(isMovable(piece, tempPiece))
				{	
					options.add(tempPiece);
				}
			}
			if(currentPieceX < Board.BOARD_COLS - 1) //if piece is not rightmost (8)
			{
				tempPiece = board.getPieces()[currentPieceX + 1][currentPieceY];
				
				if(isMovable(piece, tempPiece))
				{	
					options.add(tempPiece);
				}
			}
		}
	}
	
	public boolean isMovable(Piece piece, Piece targetPiece) //if current piece can be moved to targetPiece
	{
		return (targetPiece.isEmpty() || areOnDifferentTeams(piece, targetPiece));
	}
	
	public boolean isLegalMove(Piece piece, Piece targetPiece)
	{
		ArrayList<Piece> opts = getOptionsForPiece(piece);
		boolean availableSpace = false;
		int count = 0;
		while(!availableSpace && count < opts.size())
		{
			if(opts.get(count).getX() == targetPiece.getX() 
					&& opts.get(count).getY() == targetPiece.getY()
					&& !moveMeansGeneralInDanger(piece, targetPiece))
			{
				availableSpace = true;
			}
			count++;
		}
		
		return availableSpace;
	}
	
	public boolean areOnDifferentTeams(Piece piece1, Piece piece2)
	{
		return !piece1.isEmpty() && !piece2.isEmpty() && 
				!piece1.getTeamString().equals(piece2.getTeamString());
	}
	
	public void switchPlayerTurn()
	{
		if(currentPlayerTurn.equals("b"))
		{
			currentPlayerTurn = "r";
		}
		else
		{
			currentPlayerTurn = "b";
		}	
	}
	
	public void switchPlayerTurnAndUpdate()
	{
		switchPlayerTurn();
		currentTurnChanges.clear();
	}
	
	public void undoMove()
	{	
		if(changedPieces.size() > 0)
		{
			for(Piece p : changedPieces.get(changedPieces.size() - 1))
			{
				board.getPieces()[p.getX()][p.getY()] = new Piece(p);
				currentTurnChanges.add(p);
			}
			changedPieces.remove(changedPieces.size() - 1);
			switchPlayerTurn();
			updatePieceArrays();
			currentTurn--;
		}
	}
		
	public boolean generalEyeContact() //if no pieces are in between both generals on same y
	{	
		int x = player2General.getX();		
		
		if(player1General.getX() != x)
		{
			return false;
		}
		
		boolean passedMiddlePiece = false;
		int y = player2General.getY();
		Piece tempPiece;	
		for(int i = y; i < Board.BOARD_ROWS; i++)
		{
			tempPiece = new Piece(board.getPieces()[x][i]);
			
			if(!tempPiece.isEmpty() && !tempPiece.getTypeString().equals("b"))
			{
				passedMiddlePiece = true;
			}
			if(passedMiddlePiece && i == player1General.getY())
			{
				return false;
			}
		}
		return true;
	}	
	
	public boolean generalInDanger(Piece general)
	{
		if(general.getTypeString().equals("b"))
		{
			String teamString = "r"; //this is to get the opposite team
			if(general.getTeamString().equals("r"))
			{
				teamString = "b";
			}
			
			ArrayList<Piece> tempArrayList;
			for(Piece p : board.getTeamPieces(teamString))
			{
				if(!p.isEmpty())
				{
					tempArrayList = getOptionsForPiece(p);
					for(Piece p2 : tempArrayList)
					{
						if(p2.getX() == general.getX() && p2.getY() == general.getY())
						{
							return true;
						}
					}
				}
			}
			
		}
		
		return false;
	}
	
	public boolean moveMeansGeneralInDanger(Piece oldPiece, Piece targetPiece)
	{		
		
		String teamString = "r"; //this is to get the opposite team
		if(oldPiece.getTeamString().equals("r"))
		{
			teamString = "b";
		}
		
		Piece tempOldPiece = new Piece(oldPiece);
		Piece tempTargetPiece = new Piece(targetPiece);
		
		board.movePiece(oldPiece, targetPiece);
		
		//if oldpiece was a general
		updateGeneral(new Piece(oldPiece.getName(),targetPiece.getX(),targetPiece.getY()));
		
		Piece general; //this is currentPiece's team general
		if(teamString.equals("b"))
		{
			general = player1General;
		}
		else
		{
			general = player2General;
		}

		updatePieceArrays();
		if(generalInDanger(general))
		{
			board.getPieces()
			[tempOldPiece.getX()][tempOldPiece.getY()].setName(tempOldPiece.getName());
			board.getPieces()
			[tempTargetPiece.getX()][tempTargetPiece.getY()].setName(tempTargetPiece.getName());
			updateGeneral(tempOldPiece);
			updatePieceArrays();
			return true;
		}
		
		board.getPieces()
		[tempOldPiece.getX()][tempOldPiece.getY()].setName(tempOldPiece.getName());
		board.getPieces()
		[tempTargetPiece.getX()][tempTargetPiece.getY()].setName(tempTargetPiece.getName());
		updateGeneral(tempOldPiece);
		updatePieceArrays();
		
		
		
		/*
		//oldpiece is piece to be moved, targetpiece is destination
		String teamString = "r"; //this is to get the opposite team
		if(oldPiece.getTeamString().equals("r"))
		{
			teamString = "b";
		}
				
		Piece tempOldPiece = new Piece(oldPiece);
		Piece tempTargetPiece = new Piece(targetPiece);
		
		board.movePiece(oldPiece, targetPiece);
		
		//if oldpiece was a general
		updateGeneral(new Piece(oldPiece.getName(),targetPiece.getX(),targetPiece.getY()));
				
		Piece general; //this is currentPiece's team general
		if(teamString.equals("b"))
		{
			general = player1General;
		}
		else
		{
			general = player2General;
		}
		
		updatePieceArrays();

		ArrayList<Piece> tempArrayList;
		for(Piece p : board.getTeamPieces(teamString))
		{
			if(!p.isEmpty())
			{
				tempArrayList = getOptionsForPiece(p);
				for(Piece p2 : tempArrayList)
				{
					if(p2.getX() == general.getX() && p2.getY() == general.getY())
					{		
						board.getPieces()
						[tempOldPiece.getX()][tempOldPiece.getY()].setName(tempOldPiece.getName());
						board.getPieces()
						[tempTargetPiece.getX()][tempTargetPiece.getY()].setName(tempTargetPiece.getName());
						updateGeneral(tempOldPiece);
						updatePieceArrays();
						
						return true;
					}
				}
			}
		}
		board.getPieces()
		[tempOldPiece.getX()][tempOldPiece.getY()].setName(tempOldPiece.getName());
		board.getPieces()
		[tempTargetPiece.getX()][tempTargetPiece.getY()].setName(tempTargetPiece.getName());
		updateGeneral(tempOldPiece);
		updatePieceArrays();
*/
		return false;
	}
	
	public void updateGeneral(Piece general)
	{
		if(!general.isEmpty() && general.getTypeString().equals("b"))
		{
			if(general.getTeamString().equals("b"))
			{
				player2General = new Piece(general);
			}
			else
			{
				player1General = new Piece(general);
			}
		}
	}
	
	public void updatePieceArrays()
	{
		board.getPlayer1Pieces().clear();
		board.getPlayer2Pieces().clear();
		Piece tempPiece;
		
		for(int x = 0; x < Board.BOARD_COLS; x++)
		{
			for(int y = 0; y < Board.BOARD_ROWS; y++)
			{
				tempPiece = board.getPieces()[x][y];
				if(!tempPiece.isEmpty())
				{
					board.getTeamPieces(tempPiece.getTeamString()).add(tempPiece);
				}
			}
		}
	}
	
	public boolean checkmate(String teamString)
	{		//if general cannot escape death (being killed is possible for every move he makes)
		Piece general = player1General;
		if(teamString.equals("b"))
		{
			general = player2General;
		}
		
		ArrayList<Piece> generalOptions = getOptionsForPiece(general);
		
		for(Piece p : generalOptions)
		{
			if(!moveMeansGeneralInDanger(general, p))
			{
				return false;
			}
		}

		if(!generalInDanger(general))
		{
			return false;
		}
		
		return true;
	}
	
	public ArrayList<ArrayList<Piece>> getChangedPieces()
	{
		return changedPieces;
	}
		
	public void setPlayerTurn(String turn)
	{
		currentPlayerTurn = turn;
	}
	
	public String getPlayerTurn()
	{
		return currentPlayerTurn;
	}
	
	public ArrayList<Piece> getCurrentTurnChanges()
	{
		return currentTurnChanges;
	}
	
	public ArrayList<Piece> getOldPieces()
	{
		return oldPieces;
	}
	
	public Board getBoard()
	{
		return board;
	}
	
	public void setSelectedPiece(Piece piece)
	{
		selectedPiece = piece;
	}
	
	public Piece getSelectedPiece()
	{
		return selectedPiece;
	}
	
	public int getCurrentTurn()
	{
		return currentTurn;
	}
	
	public void setHumanPlayerTeam(String team)
	{
		humanPlayerTeam = team;
	}
	
	public String getHumanPlayerTeam()
	{
		return humanPlayerTeam;
	}

}