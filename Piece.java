public class Piece 
{
	String name; //used to get filename
	String type; //used to determine moveable areas
	int posX;
	int posY;
	//moveable spaces
	
	public Piece(){} //empty piece
	
	public Piece(String pName, int x, int y)
	{
		name = pName;
		posX = x;
		posY = y;
	}
	
	public Piece(int x, int y)
	{
		name = null;
		posX = x;
		posY = y;
	}
	
	public Piece(Piece p)
	{
		name = p.getName();
		posX = p.getX();
		posY = p.getY();
	}
	
	public void clearPiece()
	{
		name = null;
	}
	
	
	public boolean isEmpty()
	{
		return name == null;
	}
	
	public void setName(String newname)
	{
		name = newname;
	}
	
	public void setX(int val)
	{
		posX = val; 
	}
	
	public void setY(int val)
	{
		posY = val; 
	}
	
	public int getX()
	{
		return posX;
	}
	
	public int getY()
	{
		return posY;
	}
	
	public String getName()
	{
		return name;
	}
	
	public String getTeamString() // b or r
	{
		return name.substring(0,1);
	}
	
	public String getTypeString() // 2nd letter of image file
	{
		return name.substring(1,2);
	}
}
